%% settings_push2D.m

%% High-Level Steps
% # Define state and important indices
% # Set up scenario
% # Set up the plant structure
% # Set up the policy structure
% # Set up the cost structure
% # Set up the GP dynamics model structure
% # Parameters for policy optimization
% # Plotting verbosity
% # Some array initializations


%% Code
DEBUG_STUFF = false;
warning('off','all'); format short; format compact

% include some paths
try
  rd = '../../';
  addpath([rd 'base'],[rd 'util'],[rd 'gp'],[rd 'control'],[rd 'loss']);
catch
end

rand('seed',1); randn('seed',1);
% 1a. Full state representation (including all augmentations)
% - non-angle velocities
% - angular velocities
% - non-angles
% - angles
% - controls
%
% -1 dx:        x-velocity of object
% -2 dy:        y-velocity of object
% -3 dtheta:    angular-velocity of object
%  1 x:         x-position of object
%  2 y:         y-position of object
%  3 x_ee:      x-position of end effector
%  4 y_ee:      y-position of end effector
%  5 theta:     xy-orientation of object
%  6 sin(theta) complex representation of...
%  7 cos(theta) theta
%  8 v_x_ee:     x velocity of the end effector
%  9 v_y_ee:     y velocity of the end effector

% 1b. Important indices
% odei  indicies for the ode solver
% augi  indicies for variables augmented to the ode variables
% dyno  indicies for the output from the dynamics model and indicies to loss
% angi  indicies for variables treated as angles (using sin/cos representation)
% dyni  indicies for inputs to the dynamics model
% poli  indicies for variables that serve as inputs to the policy
% difi  indicies for training targets that are differences (rather than values)

odei = [1 2 3 4 5]; % Change s, change dyno, change angi, change augi
augi = []; %[3 4];
dyno = [1 2 3 4 5]; % Change target
angi = [5];
dyni = [1 2 3 4 6 7];
poli = [1 2 3 4 6 7]; % subset of odei % Change hyperparameters
difi = [1:length(dyno)];

% 2. Set up the scenario
dt = 0.15;                    % [s] sampling time
T = 15.0;                     % [s] initial prediction horizon time
H = ceil(T/dt);               % prediction steps (optimization horizon)
maxH = ceil(10.0/dt);         % max pred horizon
s = [0.01 0.01 0.01 0.01 0.01].^2;
S0 = diag(s);                 % initial state variance, 95% is +/- 11.4 degrees

mu0 = zeros(length(odei),1);  % initial state mean
N = 40;                       % number controller optimizations
if DEBUG_STUFF
    J = 2;                        % initial J trajectories of length H
else
    J = 5;                        % initial J trajectories of length H
end
K = 1;                        % number of initial states for which we optimize
nc = 10;                      % number of controller basis functions

% 3. Plant structure
plant.dynamics = @dynamics_push2D;                    % dynamics ode function
plant.noise = diag(ones(1,length(odei))*0.01.^2); % measurement noise
plant.dt = dt;
plant.ctrl = @zoh;                                % controler is zero order hold
plant.odei = odei;
plant.augi = augi;
plant.angi = angi;
plant.poli = poli;
plant.dyno = dyno;
plant.dyni = dyni;
plant.difi = difi;
plant.prop = @propagated;

% 4. Policy structure
policy.fcn = @(policy,m,s)conCat(@congp,@gSat,policy,m,s);% controller representation
policy.maxU = [1.0 1.0];                                  % max. velcoties of control
[mm ss cc] = gTrig(mu0, S0, plant.angi);                  % represent angles
mm = [mu0; mm]; cc = S0*cc; ss = [S0 cc; cc' ss];         % in complex plane
policy.p.inputs = gaussian(mm(poli), ss(poli,poli), nc)'; % init. location of basis functions
policy.p.targets = 0.1*randn(nc, length(policy.maxU));    % init. policy targets (close to zero)
policy.p.hyp = repmat(log([1 1 1 1 0.7 0.7 1 0.01]'),1,2);
% 5. Set up the cost structure
cost.fcn = @loss_push2D;                   % cost function
cost.gamma = 1;                            % discount factor (no discount since finite horizon)
cost.width = 0.5;                          % cost function width
cost.expl =  0.0;                          % exploration parameter (UCB)
cost.angle = plant.angi;                   % index of angle (for cost function)
cost.target = [1.5 -0.3 0 0 0]';           % target state % Chnage loss weights

% 6. Dynamics model structure
dynmodel.fcn = @gp1d;                % function for GP predictions
dynmodel.train = @train;             % function to train dynamics model
dynmodel.induce = zeros(300,0,1);    % shared inducing inputs (sparse GP)

% defines the max. number of line searches when training the GP dynamics models
% trainOpt(1): full GP,
% trainOpt(2): sparse GP (FITC)
if DEBUG_STUFF
    trainOpt = [300 100];
else
    trainOpt = [300 500];
end

% 7. Parameters for policy optimization
if DEBUG_STUFF
    opt.length = 5;                        % max. number of line searches
else
    opt.length = 50;                        % max. number of line searches
end
opt.MFEPLS = 30;                         % max. number of function evaluations
                                         % per line search
opt.verbosity = 3;                       % verbosity: specifies how much 
                                         % information is displayed during
                                         % policy learning. Options: 0-3

% 8. Plotting verbosity
plotting.verbosity = 2;            % 0: no plots
                                   % 1: some plots
                                   % 2: all plots

% 9. Some initializations
x = []; y = [];
fantasy.mean = cell(1,N); fantasy.std = cell(1,N);
realCost = cell(1,N); M = cell(N,1); Sigma = cell(N,1);

% 10. Initial policy for data collection
init_policy.maxU = policy.maxU;
init_policy.fcn = @centroid_alignment;
init_policy.k_contact_g = 0.1;
init_policy.k_contact_d = 0.2;
% TODO: Initialize the RBF policy parameters using these as
% training data