function dz = dynamics_ellipse(t, z, U, V)

% Simulate tabletop sliding conditions
%
% state is given by z=[x, y, x_ee, y_ee, theta]^T
%
% 1 x:         x-position of object
% 2 y:         y-position of object
% 3 x_ee:       x-position of end effector
% 4 y_ee:       y-position of end effector
% 5 theta:     xy-orientation of object

% Convert hand pose into obejct frame
x_idx = 1;
y_idx = 2;
x_ee_idx = 3;
y_ee_idx = 4;
theta_idx = 5;

% TODO: Switch here if we make the end effector in relative pose
c_x = z(x_ee_idx)-z(x_idx);
c_y = z(y_ee_idx)-z(y_idx);
theta = z(theta_idx);
v_c = [U(t);
       V(t)];

M = 1.0; % [kg] mass of the object
mu_k =  0.1;  % [N/m/s] kinect coefficient of friction between object and
            % table
mu_s =  0.1;  % [N/m/s] static coefficient of friction between object and table
g = 9.82; % [m/s^2]  acceleration of gravity
l1 = 0.5; % [m] width of object
l2 = 0.5; % [m] height of object
width = l1;
height = l2;
f_n = g*M;
f_max = mu_s*f_n;
m_max = f_max/(l1*l2*l2)*(l2*l2*l2*asinh(l1/abs(l2))+l1*l1*l1* ...
                          asinh(l2/l1)+2*l1*l2*hypot(l2,l1));
f_maxSq = f_max*f_max;
m_maxSq = m_max*m_max;

A = [2/f_maxSq 0         0;
     0         2/f_maxSq 0;
     0         0         2/m_maxSq];
C = [1 0;
     0 1;
     -c_y c_x];
B = [2*(m_maxSq/f_maxSq + c_y*c_y) -2*c_x*c_y;
     -2*c_x*c_y                    2*(m_maxSq/f_maxSq + c_x*c_x)];
M = m_max*m_max*A*C*inv(B);
R = [cos(theta) -sin(theta) 0;
     sin(theta)  cos(theta) 0;
     0           0          1];
v_o = R*M*v_c;

pusher_radius = 0.05;

dz = zeros(length(z),1);
% Change in robot finger location (always moving)
dz(x_ee_idx) = v_c(1);
dz(y_ee_idx) = v_c(2);
% Check if in contact
% Quasi-static assumption, object isn't moving without outside force
% TODO: Ensure applied force > f_n
dt = 0.15*0.5;
width_bound = 0.5*width+pusher_radius;
height_bound = 0.5*height+pusher_radius;
in_contact = (abs(c_x) < width_bound) & (abs(c_y) < height_bound);
if in_contact
    dz(x_idx) = v_o(1);
    dz(y_idx) = v_o(2);
    % Change in rotational position
    dz(theta_idx) = v_o(3);
end

