function [x, y, latent, target] = format_trajectory_data(trajectory, angi)

H = size(trajectory,1)-1;

% X format for learning:
%  1 x:         x-position of object
%  2 y:         y-position of object
%  3 x_ee:      x-position of end effector
%  4 y_ee:      y-position of end effector
%  5 theta:     xy-orientation of object
%  6 sin(theta) complex representation of...
%  7 cos(theta) theta
%  8 v_x_ee:     x velocity of the end effector
%  9 v_y_ee:     y velocity of the end effector

goal_pose = trajectory(1,[7 8 9]);

x = trajectory(:,[1 2 18 19 3]);
u = trajectory(:,[11 12]);
x(:,1) = x(:,1)-goal_pose(1);
x(:,2) = x(:,2)-goal_pose(2);
x(:,3) = x(:,3)-goal_pose(1);
x(:,4) = x(:,4)-goal_pose(2);

nX = size(x,2);
nU = size(u,2);

x = [x sin(x(:,5)) cos(x(:,5))];
y = x(2:H+1,1:nX);
latent = [x(1:H+1,1:nX) u(1:H+1,:)];
x = [x(1:H,:) u(1:H,:)];

target = zeros(nX,1);
target(1:3,1) = goal_pose.';

end