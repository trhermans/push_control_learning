function draw_rollout_push2D(latent, cost, dt)
if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
for r = 1:size(latent,1)
    draw_push2D(latent(r,:), cost)
  pause(dt);
end
