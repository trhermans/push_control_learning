%% learn_push2D.m
% 1. Initialization
clear all; close all;
settings_push2D;                     % load scenario-specific settings
basename = 'push2D_robot_';                % filename used for saving data
training_data_filename = '/home/thermans/sandbox/control_learning_data/aff_learn_out.txt';
[push_trials, trajectories] = read_robot_trials(training_data_filename);


% 2. Format training data appropriately
for jj = 1:length(trajectories)
    jj
    [xx, yy, latent{jj}, target] = format_trajectory_data(trajectories{jj}, angi);
    target
    cost.target = target;
    x = [x; xx]; y = [y; yy];       % augment training sets for dynamics model
    if plotting.verbosity > 0;      % visualization of trajectory
        draw_rollout_push2D(latent{jj}, cost, dt);
    end
end

% Force no noise gaussian noise for creating the mean here.
mu0Sim(odei,:) = x(1,odei).';
S0Sim(odei,odei) = S0;
mu0Sim = mu0Sim(dyno); S0Sim = S0Sim(dyno,dyno);

% 3. Controlled learning (N iterations)
for j=1:1
    trainDynModel;   % train (GP) dynamics model
    learnPolicy;     % learn policy
end

% Write learned controller to disk
controller_path = '/u/thermans/cfg/controllers/';
file_name = [controller_path '/RBF_' basename num2str(cputime()) '.txt'];
write_controller_data(file_name, policy);