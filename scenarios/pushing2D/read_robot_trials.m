function [push_trials, trajectories] = read_robot_trials(filename)
% Header lines to match
LEARN_TRIAL_HEADER_LINE = '# object_id/trial_id init_x init_y init_z init_theta final_x final_y final_z final_theta goal_x goal_y goal_theta push_start_point.x push_start_point.y push_start_point.z behavior_primitive controller proxy which_arm push_time precondition_method score [shape_descriptors]';
CONTROL_HEADER_LINE = '# x.x x.y x.theta x_dot.x x_dot.y x_dot.theta x_desired.x x_desired.y x_desired.theta theta0 u.linear.x u.linear.y u.linear.z u.angular.x u.angular.y u.angular.z time hand.x hand.y hand.z';
BAD_TRIAL_HEADER_LINE='#BAD_TRIAL';
% Storage variables
push_trials = {};
trajectories = {};
trial_starts = 0;
bad_stops = 0;
object_comments = 0;
control_headers = 0;
trial_number = 1;
cur_trajectory = [];
% Switches
read_pl_trial_line = false;
read_ctrl_line = false;
trial_is_start = true;
% Read in the file
file_in = fopen(filename);
cur_line = fgets(file_in);
% Loop through the lines
while ischar(cur_line)
    % Determine the kind of line
    if strfind(cur_line, LEARN_TRIAL_HEADER_LINE) == 1
        % disp('Learn trial line');
        object_comments = object_comments + 1;
        if trial_is_start
            trial_starts = trial_starts + 1;
        end
        read_pl_trial_line = true;
        read_ctrl_line = false;
    elseif strfind(cur_line, CONTROL_HEADER_LINE) == 1
        % disp('Control trial line');
        control_headers = control_headers + 1;
        read_ctrl_line = true;
    elseif strfind(cur_line, BAD_TRIAL_HEADER_LINE) == 1
        % disp('Bad trial line');
        bad_stops = bad_stops + 1;
        trial_is_start = true;
    elseif strfind(cur_line, '#') == 1
        % disp('Other comment; ignoring');
    elseif read_pl_trial_line
        % disp('Reading trial');
        trial = parse_push_trial_line(cur_line);
        read_pl_trial_line = false;
        if trial_is_start
            cur_trial{1} = trial;
            cur_trajectory = [];
        else
            cur_trial{2} = trial;
            % NOTE: Turned off because we need to have the correct hand pose for learning
            % NOTE: Uncomment to append the final pose to the trajectory
            % traj_pt = control_parse_from_trial_parse(trial)
            % cur_trajectory = [cur_trajectory; traj_pt];
            % Save data to storage stuff
            push_trials{trial_number} = cur_trial;
            trajectories{trial_number} = cur_trajectory;
            trial_number = trial_number + 1;
        end
        trial_is_start = ~trial_is_start;
    elseif read_ctrl_line
        traj_pt = parse_control_line(cur_line);
        cur_trajectory = [cur_trajectory; traj_pt];
    else
        disp('None of these?');
    end
    % Get the next line
    cur_line = fgets(file_in);
end
% Cleanup
fclose(file_in);
end

function parsed_line = parse_control_line(data_line)
parsed_line = sscanf(data_line, '%g');
parsed_line = parsed_line.';
end

function parsed_line = parse_push_trial_line(data_line)
[parsed_line, str_loc] = textscan(data_line, '%s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %s %s %s %s %f %s %f', 1);
% NOTE: Get the shape features if they exists
remainder = data_line(str_loc:end);
if length(remainder) > 2
    shape_feature = sscanf(remainder, '%f');
    parsed_line{length(parsed_line)+1} = shape_feature.';
end
end

function traj_pt = control_parse_from_trial_parse(trial)
traj_pt = [trial{6} trial{7} trial{9} 0.0 0.0 0.0 trial{10} trial{11} trial{12} trial{5} 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0];
end