function [u] = spin_compensation(policy, m, s)
    x_idx = 1;
    y_idx = 2;
    x_ee_idx = 3;
    y_ee_idx = 4;
    theta_idx = 5;

    u = zeros(1,2);
    target = [0.0 0.0 0.0 0.0 0.0]';
    centroid_x = m(x_idx);
    centroid_y = m(y_idx);
    ee_x = m(x_ee_idx);
    ee_y = m(y_ee_idx);
    % Push centroid towards the desired goal
    x_error = target(x_idx) - centroid_x;
    y_error = target(y_idx) - centroid_y;
    goal_x_dot = policy.k_contact_g*x_error;
    goal_y_dot = policy.k_contact_g*y_error;

    % Add in direction to corect for not pushing through the centroid
    m = (((ee_x - centroid_x)*x_error + (ee_y - centroid_y)*y_error)./ sqrt(x_error*x_error + y_error*y_error));
    tan_pt_x = centroid_x + m*x_error;
    tan_pt_y = centroid_y + m*y_error;
    contact_pt_x_dot = policy.k_contact_d*(tan_pt_x - ee_x);
    contact_pt_y_dot = policy.k_contact_d*(tan_pt_y - ee_y);

    u(1) = max(min(goal_x_dot + contact_pt_x_dot, policy.maxU(1)),-policy.maxU(1));
    u(2) = max(min(goal_y_dot + contact_pt_y_dot, policy.maxU(2)),-policy.maxU(2));
