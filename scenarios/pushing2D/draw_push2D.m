function draw_push2D(X, cost, s, s2, M, S)
% draw the table push system with reward and applied force
%
% (C) Copyright 2013 Tucker Hermans
% (C) Copyright 2009-2011 Carl Edward Rasmussen and Marc Deisenroth
% 2013-01-15

x_idx = 1;
y_idx = 2;
ee_x_idx = 3;
ee_y_idx = 4;
theta_idx = 5;

x = X(x_idx)+cost.target(x_idx);
y = X(y_idx)+cost.target(y_idx);
x_ee = X(ee_x_idx)+cost.target(x_idx);
y_ee = X(ee_y_idx)+cost.target(y_idx);
theta = X(theta_idx);
v_ee_x = X(6);
v_ee_y = X(7);
N=5;

scale = 1;
font_size = 10;

xmin = -3*scale;
xmax = 3*scale;
ymin = -3*scale;
ymax = 3*scale;
height = 0.25*scale;
width  = 0.25*scale;

table_ymin = -2*scale;
table_ymax = ymax;

% Compute positions
% TODO: Rotate based on theta
% TODO: Draw line in theta direction
rot_theta = [cos(theta) -sin(theta);
             sin(theta) cos(theta)];
obj = [ width,  height;
        width, -height;
       -width, -height;
       -width,  height;
        width,  height].';

obj_rot = rot_theta*obj;
obj_rot = obj_rot.';
obj_rot(:,1) = obj_rot(:,1)+x;
obj_rot(:,2) = obj_rot(:,2)+y;
obj_tip = [(obj_rot(1,1)+obj_rot(4,1))*0.5;
           (obj_rot(1,2)+obj_rot(4,2))*0.5];
clf
hold on

% Draw table boundaries
fill([xmin, xmax, xmax, xmin, xmin], [table_ymin, table_ymin, table_ymax, table_ymax, table_ymin], 'w');
plot([xmin, xmax], [table_ymin, table_ymin], 'Color','b','LineWidth',3);
plot([xmin, xmax], [table_ymax, table_ymax], 'Color','b','LineWidth',3);
plot([xmin, xmin], [table_ymin, table_ymax], 'Color','b','LineWidth',3);
plot([xmax, xmax], [table_ymin, table_ymax], 'Color','b','LineWidth',3);

% Draw Obj
plot(obj_rot(:,1), obj_rot(:,2),'Color','k','LineWidth',3);
fill(obj_rot(:,1), obj_rot(:,2),'k');
plot([x, obj_tip(1)], [y, obj_tip(2)], 'Color', 'r', 'LineWidth', 3);

% Draw pusher
plot([x_ee, x_ee],[y_ee-0.05*scale, y_ee+0.05*scale],'Color','g','LineWidth',3);
plot([x_ee-0.05*scale, x_ee+0.05*scale],[y_ee,y_ee],'Color','g','LineWidth',3);

% Draw target location
plot([cost.target(x_idx), cost.target(x_idx)],[cost.target(y_idx)-0.05*scale, ...
		    cost.target(y_idx)+0.05*scale],'Color','r','LineWidth',3);
plot([cost.target(x_idx)-0.05*scale, cost.target(x_idx)+0.05*scale],[cost.target(y_idx),cost.target(y_idx)],'Color','r','LineWidth',3);

% Draw reward
% TODO: Fix this
reward = 1-cost.fcn(cost, X(1:length(1:N)).', eye(N));
plot([0 reward*xmax],[-2.5, -2.5].*scale, 'Color', 'y', 'LineWidth', font_size);
% Draw exerted force
plot([0 v_ee_x/10*xmax],[-2.7, -2.7].*scale, 'Color', 'g', 'LineWidth', font_size);
plot([0 v_ee_y/10*xmax],[-2.9, -2.9].*scale, 'Color', 'g', ...
     'LineWidth', font_size);

% TODO: Plot location uncertainty as ellipse
try
    S1 = S(1:2,1:2);
    M1 = M(1:2);
    error_ellipse(S1, M1,'style','b');
catch
end


text(0,-2.5*scale,'immediate reward','fontsize', font_size)
text(0,-2.7*scale,'applied  velocity (x)','fontsize', font_size)
text(0,-2.9*scale,'applied  velocity (y)','fontsize', font_size)
if exist('s','var')
  text(0,-2.1*scale, s,'fontsize', font_size)
end
if exist('s2','var')
  text(0,-2.3*scale, s2,'fontsize', font_size)
end

set(gca,'DataAspectRatio',[1 1 1],'XLim',[xmin xmax],'YLim',[ymin ymax].*scale);
axis off;

drawnow;
