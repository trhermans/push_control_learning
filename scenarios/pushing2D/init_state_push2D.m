function mu0 = init_state_push2D(mu0, S0, target)
x_idx = 1;
y_idx = 2;
x_ee_idx = 3;
y_ee_idx = 4;
theta_idx = 5;

% Get gaussian samples for (x,y,theta) if have nonzero S0
if sum(S0(:)) > 0
    mu0 = gaussian(mu0, S0);
end
% Make the hand pose deterministic given the target and start pose
hand_angle = atan2(-target(y_idx) + mu0(y_idx), -target(x_idx) + mu0(x_idx));
l1 = 0.5;
l2 = 0.5;
pusher_radius = 0.05;
% TODO: Compute exact intersection location using local theta and distances
offset = pusher_radius + 0.2;
mu0(x_ee_idx) = cos(hand_angle)*(l1*0.5+offset)+mu0(x_idx); % x_hand
mu0(y_ee_idx) = sin(hand_angle)*(l2*0.5+offset)+mu0(y_idx); % y_hand

% Transform goal to be the origin
mu0(x_idx) = mu0(x_idx) - target(x_idx);
mu0(y_idx) = mu0(y_idx) - target(y_idx);
mu0(x_ee_idx) = mu0(x_ee_idx) - target(x_idx);
mu0(y_ee_idx) = mu0(y_ee_idx) - target(y_idx);
