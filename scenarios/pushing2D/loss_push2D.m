%% loss_push2D.m
function [L, dLdm, dLds, S2] = loss_push2D(cost, mu, Sigma)

x_idx = 1;
y_idx = 2;

cw  = cost.width;
try
  b = cost.expl;
catch
  b = 0;
end
if isempty(b); b = 0; end

D0 = size(Sigma,2); % state dimension
D1 = D0 + 2*length(cost.angle);

M = zeros(D1,1);
M(1:D0) = mu;
S = zeros(D1);
S(1:D0,1:D0) = Sigma;
Mdm = [eye(D0); zeros(D1-D0,D0)];
Sdm = zeros(D1*D1,D0);
Mds = zeros(D1,D0*D0);
Sds = kron(Mdm,Mdm);

iT = zeros(D1);   % construct distance matrix
iT(x_idx, x_idx)=1;
iT(y_idx, y_idx)=1;

% HACK: Target is set to 0, since we transform coordinates into the target frame to generalize learning
target = zeros(size(cost.target));

% Augment state with angle information

i = 1:D0;
k = D0+1:D1;
% Augment state
[M(k), S(k,k), C, mdm, sdm, Cdm, mds, sds, Cds] = gTrig(mu, Sigma, cost.angle);
% Augment target
z = [target(:); gTrig(target, zeros(D0), cost.angle)];
% compute derivatives (for augmentation)
X = reshape(1:D1*D1,[D1 D1]); XT = X';              % vectorized indices
I=0*X; I(i,i)=1; ii=X(I==1)'; I=0*X; I(k,k)=1; kk=X(I==1)';
I=0*X; I(i,k)=1; ik=X(I==1)'; ki=XT(I==1)';

Mdm(k,:)  = mdm*Mdm(i,:) + mds*Sdm(ii,:);                    % chainrule
Mds(k,:)  = mdm*Mds(i,:) + mds*Sds(ii,:);
Sdm(kk,:) = sdm*Mdm(i,:) + sds*Sdm(ii,:);
Sds(kk,:) = sdm*Mds(i,:) + sds*Sds(ii,:);
dCdm      = Cdm*Mdm(i,:) + Cds*Sdm(ii,:);
dCds      = Cdm*Mds(i,:) + Cds*Sds(ii,:);

S(i,k) = S(i,i)*C; S(k,i) = S(i,k)';                      % off-diagonal
SS = kron(eye(length(k)),S(i,i)); CC = kron(C',eye(length(i)));
Sdm(ik,:) = SS*dCdm + CC*Sdm(ii,:); Sdm(ki,:) = Sdm(ik,:);
Sds(ik,:) = SS*dCds + CC*Sds(ii,:); Sds(ki,:) = Sds(ik,:);

% Calculate loss!
L = 0; dLdm = zeros(1,D0); dLds = zeros(1,D0*D0); S2 = 0;
for i = 1:length(cw)                    % scale mixture of immediate costs
    cost.z = z; cost.W = iT/cw(i)^2;
    [r rdM rdS s2 s2dM s2dS] = lossSat(cost, M, S);

    L = L + r; S2 = S2 + s2;
    dLdm = dLdm + rdM(:)'*Mdm + rdS(:)'*Sdm;
    dLds = dLds + rdM(:)'*Mds + rdS(:)'*Sds;

    if (b~=0 || ~isempty(b)) && abs(s2)>1e-12
        L = L + b*sqrt(s2);
        dLdm = dLdm + b/sqrt(s2) * ( s2dM(:)'*Mdm + s2dS(:)'*Sdm )/2;
        dLds = dLds + b/sqrt(s2) * ( s2dM(:)'*Mds + s2dS(:)'*Sds )/2;
    end
end

% normalize
L = L/length(cw);
dLdm = dLdm/length(cw);
dLds = dLds/(length(cw));
S2 = s2/(length(cw));