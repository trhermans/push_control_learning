%% learn_push2D.m
% 1. Initialization
clear all; close all;
settings_push2D;                     % load scenario-specific settings
basename = 'push2D_';                % filename used for saving data

% 2. Initial J random rollouts
for jj = 1:J
    jj
    mu0_generated = init_state_push2D(mu0, S0, cost.target);
    [xx, yy, realCost{jj}, latent{jj}] = rollout(mu0_generated, init_policy, H, plant, cost);
    x = [x; xx]; y = [y; yy];       % augment training sets for dynamics model
    if plotting.verbosity > 0;      % visualization of trajectory
        draw_rollout_push2D(latent{jj}, cost, dt);
    end
end

% Force no noise gaussian noise for creating the mean here.
mu0Sim(odei,:) = init_state_push2D(mu0, zeros(size(S0)), cost.target);
S0Sim(odei,odei) = S0;
mu0Sim = mu0Sim(dyno); S0Sim = S0Sim(dyno,dyno);

% 3. Controlled learning (N iterations)
for j = 1:N
  trainDynModel;   % train (GP) dynamics model
  learnPolicy;     % learn policy
  applyController; % apply controller to system
  disp(['controlled trial # ' num2str(j)]);
  if plotting.verbosity > 0;      % visualization of trajectory
      draw_rollout_push2D(latent{j}, cost, dt);
  end
end

% Write learned controller to disk
controller_path = '/u/thermans/cfg/controllers/';
file_name = [controller_path '/RBF_' basename num2str(cputime()) '.txt'];
write_controller_data(file_name, policy);