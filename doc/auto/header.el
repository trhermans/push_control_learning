(TeX-add-style-hook "header"
 (lambda ()
    (TeX-add-symbols
     '("pdiff" 2)
     '("te" 1)
     '("gaussx" 3)
     '("gauss" 2)
     '("mat" 1)
     "cmark"
     "xmark"
     "inv"
     "T"
     "E"
     "var"
     "cov"
     "R"
     "Z"
     "polpar"
     "dpp")
    (TeX-run-style-hooks
     "3dplot"
     "tikz"
     "pifont"
     "listings"
     "hyperref"
     "wrapfig"
     "subfigure"
     "url"
     "units"
     "textcomp"
     "xcolor"
     "table"
     "color"
     "amsmath"
     "amssymb"
     "mathtools"
     "dsfont"
     ""
     "geometry"
     "right=2cm"
     "left=2cm"
     "bottom=3cm"
     "top=4cm")))

