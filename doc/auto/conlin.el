(TeX-add-style-hook "conlin"
 (lambda ()
    (TeX-run-style-hooks
     "listings"
     "times"
     "textcomp"
     "geometry"
     "color"
     "graphicx"
     "latex2e"
     "art11"
     "article"
     "11pt")))

