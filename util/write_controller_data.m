function write_controller_data(file_name, policy)
M = length(policy.p.hyp);
N = size(policy.p.targets, 1); % Targets = w
Wn = size(policy.p.targets, 2);
Pn = size(policy.p.inputs, 2);
fid = fopen(file_name,'w');
fprintf(fid, 'M %g\n', M);
fprintf(fid, 'N %g\n', N);
fprintf(fid, 'E %g\n', Wn);
fprintf(fid, 'D %g\n', Pn);
fprintf(fid, 'maxU ');
fprintf(fid, '%g ', policy.maxU);
fprintf(fid','\n');
fprintf(fid,'%g ', policy.p.hyp);
fprintf(fid','\n');
for r=1:N
    fprintf(fid','%g ', policy.p.targets(r,:));
    fprintf(fid','\n');
end
for r=1:N
    fprintf(fid','%g ', policy.p.inputs(r,:));
    fprintf(fid','\n');
end
fclose(fid);
end